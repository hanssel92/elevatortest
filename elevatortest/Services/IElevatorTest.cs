﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using elevatortest.Models;

namespace elevatortest.Services
{
    public interface IElevatorTest
    {
        List<PersonQueueingModel> FillDataPersonQueue();

        List<PersonQueueingModel> CreatePersonElevatorList(string create);
    }
}
