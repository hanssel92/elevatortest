﻿using elevatortest.Models;
using elevatortest.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace elevatortest.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IElevatorTest _services;
        private readonly int _maxweight = 1000;

        public HomeController(ILogger<HomeController> logger, IElevatorTest services)
        {
            _logger = logger;
            _services = services;
        }

        public IActionResult Index(string create)
        {
            List<PersonQueueingModel> PQ = new List<PersonQueueingModel>();
            List<PersonQueueingModel> PQList = new List<PersonQueueingModel>();
            decimal sumweigth = 0;
            create = "t";
            try
            {
                PQ  = _services.FillDataPersonQueue();
               
                if (!string.IsNullOrEmpty(create))
                {
                    foreach (var item in PQ.OrderBy(a => a.turn))
                    {
                        sumweigth += item.weigth;
                        if (sumweigth <= 1000 )
                        {
                            PQList.Add(item);
                        }
                    }
                    return View(PQList);
                }

            }
            catch (Exception)
            {

                throw;
            }
            return View(PQ);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
